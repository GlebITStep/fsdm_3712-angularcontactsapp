import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  
  constructor(
    public auth: AuthService,
    private router: Router) {
    
  }

  logout() {
    this.auth.logout();
    location.href = '';
    //this.router.navigate(['']);
  }
}
