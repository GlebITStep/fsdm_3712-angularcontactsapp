import { Component, OnInit, Input } from '@angular/core';
import { Contact } from 'src/app/models/contact';
import { HttpClient } from '@angular/common/http';
import { ContactsApiService } from 'src/app/services/contacts-api.service';
import { ContactsStorageService } from 'src/app/services/contacts-storage.service';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss']
})
export class ContactItemComponent {
  @Input() contact: Contact;

  constructor(
    private contactsApi: ContactsApiService,
    private contactsStorage: ContactsStorageService) { }

  async onDelete() {
    try {
      this.contactsApi.remove(this.contact.id); 
      this.contactsStorage.remove(this.contact.id);
    } catch (error) {
      console.log(error);  
    }
  }

  async onFavorite() {
    this.contact.isFavorite = !this.contact.isFavorite;
    this.contactsApi.edit(this.contact.id, this.contact);
  }

  async onEdit() {

  }
}
