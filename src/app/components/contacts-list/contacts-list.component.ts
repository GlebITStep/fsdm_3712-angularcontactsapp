import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Contact } from 'src/app/models/contact';
import { ContactsApiService } from 'src/app/services/contacts-api.service';
import { ContactsStorageService } from 'src/app/services/contacts-storage.service';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss']
})
export class ContactsListComponent implements OnInit {
  constructor(
    private contactsApi: ContactsApiService,
    public contactsStorage: ContactsStorageService) { }

  async ngOnInit() {
    this.contactsStorage.contacts.next(await this.contactsApi.get());
  }
}
