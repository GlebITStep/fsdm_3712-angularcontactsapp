import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  email = '';
  password = '';

  constructor(
    private auth: AuthService,
    private router: Router) { }

  async onSubmit() {
    try {
      await this.auth.login(this.email, this.password);
      this.router.navigate(['contacts']);
    } catch (error) {
      console.log(error);  
    }
  }

}
