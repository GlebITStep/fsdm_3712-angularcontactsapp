import { Component, OnInit } from '@angular/core';
import { Contact } from 'src/app/models/contact';
import { ContactsApiService } from 'src/app/services/contacts-api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.scss']
})
export class ContactPageComponent implements OnInit {
  contact: Contact;

  constructor(
    private contactsApi: ContactsApiService,
    private route: ActivatedRoute
  ) { }

  async ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.contact = await this.contactsApi.getById(parseInt(id));
  }

}
