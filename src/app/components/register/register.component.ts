import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  newUser: User = {};

  constructor(
    private auth: AuthService,
    private router: Router) { }

  async onSubmit() {
    try {
      await this.auth.register(this.newUser);
      this.router.navigate(['login']);
    } catch (error) {
      console.log(error);  
    }
  }

}
