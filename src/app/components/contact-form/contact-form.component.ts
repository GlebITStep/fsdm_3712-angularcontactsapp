import { Component, OnInit } from '@angular/core';
import { Contact } from 'src/app/models/contact';
import { HttpClient } from '@angular/common/http';
import { ContactsApiService } from 'src/app/services/contacts-api.service';
import { ContactsStorageService } from 'src/app/services/contacts-storage.service';
 
@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  newContact: Contact = {}

  constructor(
    private contactsApi: ContactsApiService,
    private contactsStorage: ContactsStorageService) { }

  ngOnInit() {
  }

  async onSumbit() {
    try {
      let contact = await this.contactsApi.create(this.newContact);
      this.contactsStorage.add(contact);
    } catch (error) {
      console.log(error); 
    }
    this.newContact = {}; 
  }
}
