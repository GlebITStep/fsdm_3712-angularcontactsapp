import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Contact } from '../models/contact';

@Injectable({
  providedIn: 'root'
})
export class ContactsStorageService {
  contacts = new BehaviorSubject<Array<Contact>>([]);

  constructor() {

  }

  add(contact: Contact) {
    this.contacts.next([...this.contacts.getValue(), contact]);
  }

  remove(id: number): void {
    let currentContacts = this.contacts.getValue();
    this.contacts.next(currentContacts.filter(x => x.id != id));
  }
}


    // //... spread operator
    // let arr1 = [1, 2, 3];
    // let arr2 = [8, 9];
    // let arr3 = [...arr1, ...arr2]; //[1, 2, 3, 8, 9]
    // arr3 = [...arr3, 5]; //[1, 2, 3, 8, 9, 5]
