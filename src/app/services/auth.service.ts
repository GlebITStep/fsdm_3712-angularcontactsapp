import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Token } from '../models/token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  readonly apiUrl = 'http://localhost:61035';

  constructor(private httpClient: HttpClient) { }

  register(user: User): Promise<any>  {
    return this.httpClient.post(`${this.apiUrl}/api/accounts/register`, user).toPromise();
  }

  async login(email: string, password: string) {
    let data = {
      email: email,
      password: password
    };

    try {
      let token = await this.httpClient.post<Token>(`${this.apiUrl}/api/accounts/login`, data).toPromise();
      let json = JSON.stringify(token);
      localStorage.setItem('token', json);
    } catch (error) {
      throw error; 
    }
  }

  logout() {
    localStorage.removeItem('token');
  }

  isAuthenticated(): boolean {
    if (localStorage.getItem('token'))
      return true;
    return false;
  }

  getUsername() : string {
    let json = localStorage.getItem('token');
    if (json) {
      let myToken = JSON.parse(json) as Token;
      let base64string = myToken.token.split('.')[1];
      let userJson = atob(base64string);
      return JSON.parse(userJson).sub;
    } else {
      return 'Guest';
    }
  }

  getToken() : string {
    let json = localStorage.getItem('token');
    if (json) {
      let myToken = JSON.parse(json) as Token;
      return myToken.token;
    } else {
      return '';
    }
  }
}
