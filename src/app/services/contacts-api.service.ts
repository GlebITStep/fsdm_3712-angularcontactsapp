import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Contact } from '../models/contact';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ContactsApiService {
  readonly apiUrl = 'http://localhost:61035';
  headers: HttpHeaders;

  constructor(
    private httpClient: HttpClient,
    private auth: AuthService) {
      console.log(this.auth.getToken()); 
      this.headers = new HttpHeaders().set('Authorization', `Bearer ${this.auth.getToken()}`);
    }

  get(): Promise<Array<Contact>> {
    return this.httpClient.get<Array<Contact>>(`${this.apiUrl}/api/contacts`, { headers : this.headers }).toPromise();
  }

  getById(id: number): Promise<Contact> {
    return this.httpClient.get<Contact>(`${this.apiUrl}/api/contacts/${id}`, { headers : this.headers }).toPromise();
  }

  create(contact: Contact): Promise<Contact> {
    return this.httpClient.post<Contact>(`${this.apiUrl}/api/contacts`, contact, { headers : this.headers }).toPromise();
  }

  edit(id: number, contact: Contact): Promise<Contact> {
    return this.httpClient.put<Contact>(`${this.apiUrl}/api/contacts/${id}`, contact, { headers : this.headers }).toPromise();
  }

  remove(id: number): Promise<any> {
    return this.httpClient.delete(`${this.apiUrl}/api/contacts/${id}`, { headers : this.headers }).toPromise();
  }
}
